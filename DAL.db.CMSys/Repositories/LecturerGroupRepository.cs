﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class LecturerGroupRepository : Repository<LecturerGroup, Guid>, ILecturerGroupRepository
    {
        private CmSysContext _cmSysContext;

        public LecturerGroupRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}