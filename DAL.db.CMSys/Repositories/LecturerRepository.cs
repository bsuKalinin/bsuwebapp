﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.UserEntities;

namespace DAL.db.CMSys.Repositories
{
    public class LecturerRepository : Repository<Lecturer, Guid>, ILecturerRepository
    {
        private CmSysContext _cmSysContext;

        public LecturerRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}