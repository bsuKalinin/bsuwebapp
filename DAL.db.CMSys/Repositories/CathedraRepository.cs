﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.CourseEntities;

namespace DAL.db.CMSys.Repositories
{
    public class DepartmentRepository : Repository<Department, Guid>, IDepartmentRepository
    {
        private CmSysContext _cmSysContext;

        public DepartmentRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}