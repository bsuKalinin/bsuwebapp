﻿using System;
using DAL.Contract.IRepositories;
using Domain.Entities.CourseEntities;

namespace DAL.db.CMSys.Repositories
{
    public class CourseRepository : Repository<Course, Guid>, ICourseRepository
    {
        private readonly CmSysContext _cmSysContext;

        public CourseRepository(CmSysContext cmSysContext) : base(cmSysContext)
        {
            _cmSysContext = cmSysContext;
        }
    }
}