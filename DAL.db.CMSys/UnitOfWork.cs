﻿using System;
using System.Data;
using DAL.Contract;
using DAL.Contract.IRepositories;
using DAL.db.CMSys.Repositories;

namespace DAL.db.CMSys
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CmSysContext _cmSysContext;
        private IDepartmentRepository _Departments;
        private ICourseRepository _courses;
        private IRoleRepository _roles;
        private ILecturerGroupRepository _LecturerGroups;
        private ILecturerRepository _Lecturers;
        private IUserRepository _users;

        public UnitOfWork(CmSysContext cmSysContext)
        {
            _cmSysContext = cmSysContext ?? throw new ArgumentNullException(nameof(cmSysContext));
        }

        public IUserRepository Users => _users = _users ?? new UserRepository(_cmSysContext);
        public ICourseRepository Courses => _courses = _courses ?? new CourseRepository(_cmSysContext);
        public IRoleRepository Roles => _roles = _roles ?? new RoleRepository(_cmSysContext);
        public ILecturerRepository Lecturers => _Lecturers = _Lecturers ?? new LecturerRepository(_cmSysContext);
        public ILecturerGroupRepository LecturerGroups => _LecturerGroups = _LecturerGroups ?? new LecturerGroupRepository(_cmSysContext);
        public IDepartmentRepository Departments => _Departments = _Departments ?? new DepartmentRepository(_cmSysContext);

        public void Commit()
        {
            try
            {
                _cmSysContext.SaveChanges();
            }

            catch (Exception e)
            {
                throw new DataException("Commiting error", e);
            }
        }

        public void Dispose()
        {
            _cmSysContext.Dispose();
        }
    }
}