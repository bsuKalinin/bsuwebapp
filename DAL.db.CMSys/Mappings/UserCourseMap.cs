﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class UserCourseMap : IEntityTypeConfiguration<UserCourse>
    {
        public void Configure(EntityTypeBuilder<UserCourse> builder)
        {
            builder.HasKey(e => new { UserId = e.UserEmail, e.CourseId })
                .HasName("PK_UserCourse_xref_UserEmail_CourseId");

            builder.ToTable("UserCourse_xref");

            builder.HasOne(d => d.Course)
                .WithMany(p => p.UserCourse)
                .HasForeignKey(d => d.CourseId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.User)
                .WithMany(p => p.UserCourse)
                .HasPrincipalKey(d => d.Email)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}