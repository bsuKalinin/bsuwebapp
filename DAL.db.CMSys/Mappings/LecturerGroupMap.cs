﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class LecturerGroupMap : IEntityTypeConfiguration<LecturerGroup>
    {
        public void Configure(EntityTypeBuilder<LecturerGroup> builder)
        {
            builder.HasIndex(e => e.Name)
                .HasName("AK_LecturerGroup_Name")
                .IsUnique();

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("LecturerGroupId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(32);
        }
    }
}