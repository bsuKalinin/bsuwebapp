﻿using Domain.Entities.CourseEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class LecturerCourseMap : IEntityTypeConfiguration<LecturerCourse>
    {
        public void Configure(EntityTypeBuilder<LecturerCourse> builder)
        {
            builder.HasKey(e => new {LecturerId = e.UserId, e.CourseId})
                .HasName("PK_LecturerCourse_xref_UserId_CourseId");

            builder.ToTable("LecturerCourse_xref");

            builder.HasOne(d => d.Course)
                .WithMany(p => p.LecturerCourse)
                .HasForeignKey(d => d.CourseId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LecturerCourse_xref_Course_CourseId");

            //builder.HasOne(d => d.Lecturer)
            //    .WithMany(p => p.LecturerCourse)
            //    .HasForeignKey(d => d.UserId)
            //    .OnDelete(DeleteBehavior.ClientSetNull)
            //    .HasConstraintName("FK_LecturerCourse_xref_Lecturer_UserId");
        }
    }
}