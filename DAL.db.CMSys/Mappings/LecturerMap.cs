﻿using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.db.CMSys.Mappings
{
    public class LecturerMap : IEntityTypeConfiguration<Lecturer>
    {
        public void Configure(EntityTypeBuilder<Lecturer> builder)
        {
            builder.HasKey(e => e.UserId)
                .HasName("PK_Lecturer_UserId");

            builder.Property(e => e.UserId)
                .ValueGeneratedNever();

            builder.Property(e => e.About)
                .HasMaxLength(4000);

            builder.HasOne(d => d.Group)
                .WithMany(p => p.Lecturer)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(d => d.User)
                .WithOne(p => p.Lecturer)
                .HasForeignKey<Lecturer>(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}