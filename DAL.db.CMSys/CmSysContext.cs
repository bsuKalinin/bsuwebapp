﻿using DAL.db.CMSys.Mappings;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;
using Microsoft.EntityFrameworkCore;

namespace DAL.db.CMSys
{
    public class CmSysContext : DbContext
    {
        public CmSysContext(DbContextOptions<CmSysContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Lecturer> Lecturer { get; set; }
        public virtual DbSet<LecturerCourse> LecturerCourse { get; set; }
        public virtual DbSet<LecturerGroup> LecturerGroup { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<UserCourse> UserCourse { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CourseMap());

            modelBuilder.ApplyConfiguration(new DepartmentMap());

            modelBuilder.ApplyConfiguration(new RoleMap());

            modelBuilder.ApplyConfiguration(new LecturerMap());

            modelBuilder.ApplyConfiguration(new LecturerCourseMap());

            modelBuilder.ApplyConfiguration(new LecturerGroupMap());

            modelBuilder.ApplyConfiguration(new UserMap());

            modelBuilder.ApplyConfiguration(new UserRoleMap());

            modelBuilder.ApplyConfiguration(new UserCourseMap());

        }
    }
}