﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using BLL.Contract;
using CMSys_project.Helpers;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Controllers
{
    [Route("Account")]
    public class AccountController : Controller
    {
        private readonly IMapper _iMapper;
        private readonly IMembershipService _iMembershipService;

        public AccountController(IMembershipService iMembershipService, IMapper iMapper)
        {
            _iMembershipService = iMembershipService;
            _iMapper = iMapper;
        }

        [Route("Login")]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("Login")]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginVM model)
        {

            User user = _iMembershipService.GetAllUsers().First(x => x.Email.Equals(model.Email));
            UserVM userVm = _iMapper.Map<User, UserVM>(user);
            userVm.RoleVm = _iMapper.Map<IEnumerable<Role>, IEnumerable<RoleVM>>(user.UserRole.Select(x => x.Role))
                .ToList();
            if (_iMembershipService.EqualPasswords(user, model.Password))
            {
                Authenticate(userVm);
                Constant.CurrentName = userVm.FullName;
                if (userVm.RoleVm.Exists(x => x.Name.Equals("Administrator")))
                {
                    return RedirectToAction("Info", "AdminCourse", new {area = "Admin"});
                }

                if (userVm.RoleVm.Exists(x => x.Name.Equals("Lecturer")))
                {
                    ///throw new System.Exception();
                    return RedirectToAction("Info", "Course", new { area = "Lecturer" });
                }

                if (userVm.RoleVm.Exists(x => x.Name.Equals("User")))
                {
                    return RedirectToAction("Info", "UserCourse", new {area = "User"});
                }
            }
            
            return View(model);
        }

        private void Authenticate(UserVM user)
        {
            List<Claim> newClaim = new List<Claim>();
            if (user.RoleVm.Select(x => x.Name).Contains("Administrator"))
            {
                newClaim.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "Administrator"));
                newClaim.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString(), ClaimValueTypes.String));
            }

            
            else if (user.RoleVm.Select(x => x.Name).Contains("Lecturer"))
            {
                newClaim.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "Lecturer"));
                newClaim.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString(), ClaimValueTypes.String));
            }
            else if (user.RoleVm.Select(x => x.Name).Contains("User"))
            {
                newClaim.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "User"));
                newClaim.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString(), ClaimValueTypes.String));
            }


            newClaim.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email));

            ClaimsIdentity id = new ClaimsIdentity(newClaim, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public ActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}