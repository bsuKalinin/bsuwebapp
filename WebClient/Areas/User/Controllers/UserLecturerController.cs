﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User, Administrator, Lecturer")]
    [Route("User/Lecturer")]
    public class UserLecturerController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public UserLecturerController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<LecturerVM> LecturerVMs =
                _iMapper.Map<IEnumerable<Lecturer>, IEnumerable<LecturerVM>>(_iCatalogService.GetAllLecturer());
           
            return View("Info", LecturerVMs);
        }

 
        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Lecturer Lecturer = _iCatalogService.GetLecturer(id);
            ViewData.Model = _iMapper.Map<Lecturer, LecturerVM>(Lecturer);
            if (Lecturer is null)
            {
                ViewBag.Message = "Lecturer was not found";
                return View("Error");
            }
            return View();

        }
    }
}