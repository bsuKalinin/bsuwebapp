﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using BLL.Contract;
using CMSys_project.Helpers;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User, Administrator, Lecturer")]
    [Route("")]
    [Route("User/Course")]
    public class UserCourseController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMembershipService _iMembershipService;
        private readonly IMapper _iMapper;

        public UserCourseController(ICatalogService iCatalogService, IMapper iMapper, IMembershipService iMembershipService)
        {
            _iCatalogService = iCatalogService;
            _iMembershipService = iMembershipService;
            _iMapper = iMapper;
        }
        [Route("")]
        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<CourseVM> DepartmentVms =
                _iMapper.Map<IEnumerable<Course>, IEnumerable<CourseVM>>(_iCatalogService.GetAllCourse());
            return View("Info", DepartmentVms);

        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }

        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Course course = _iCatalogService.GetCourse(id);
            ViewData.Model = _iMapper.Map<Course, CourseVM>(course);
            if (course is null)
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }

            return View();
        }

        [Route("Subscribe/{id}")]
        [HttpGet]
        [ActionName("Subscribe")]
        public IActionResult SubscribeInfo(Guid id)
        {
            Course course = _iCatalogService.GetCourse(id);
            CourseVM courseVm = _iMapper.Map<Course, CourseVM>(course);
            if (course is null)
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }
            return View(courseVm);
        }

        [Route("Subscribe/{id}")]
        [HttpPost]
        public IActionResult Subscribe(Guid idd)
        {
            string id = User.Claims.First(x => x.Type == ClaimTypes.Name).Value;
            Domain.Entities.UserEntities.User user = _iMembershipService.GetUser(Guid.Parse(id));
            Course course = _iCatalogService.GetCourse(idd);
            foreach (LecturerCourse lecturerCourse in course.LecturerCourse)
            {
                SNS.SendMessageAsync("senderfromapp@gmail.com", "feria8000@gmail.com", user.Email, user.Name + " " + user.Surname, user.Faculty, course.Name,
                    "12345678_k");
            }

            return RedirectToAction("Info");
        }

    }
}