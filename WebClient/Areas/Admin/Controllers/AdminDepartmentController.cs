﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Department")]
    [Authorize(Roles = "Administrator")]
    public class AdminDepartment : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public AdminDepartment(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<DepartmentVM> DepartmentVms =
               _iMapper.Map<IEnumerable<Department>, IEnumerable<DepartmentVM>>(_iCatalogService.GetAllDepartment());
            return View("Info", DepartmentVms);
        }

        [Route("Create")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(DepartmentVM Department)
        {
            if (!ModelState.IsValid)
            {
                return View(Department);
            }
            Department.Id = Guid.NewGuid();
            Department courseGr = _iMapper.Map<DepartmentVM, Department>(Department);
            try
            {
                _iCatalogService.AddDepartment(courseGr);
                return RedirectToAction("Info");

            }
            catch
            {

                ViewBag.Message = $"Department with name {Department.Name} is already existed";
                return View("Error");
            }

        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            Department Department = _iCatalogService.GetDepartment(id);
            DepartmentVM courseGr = _iMapper.Map<Department, DepartmentVM>(Department);
            if (Department != null)
            {
                return View(courseGr);
            }

            ViewBag.Message = "Department was not found";
            return View("Error");


        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(DepartmentVM Department)
        {
            if (!ModelState.IsValid)
            {
                return View(Department);
            }
            Department courseGr = _iMapper.Map<DepartmentVM, Department>(Department);
            try
            {
                _iCatalogService.EditDepartment(courseGr);
                return RedirectToAction("Info");
            }
            catch
            {
                ViewBag.Role = "Admin";
                ViewBag.Message = $"Department with name {Department.Name} is already existed";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            {
                Department Department = _iCatalogService.GetDepartment(id);
                DepartmentVM DepartmentVm = _iMapper.Map<Department, DepartmentVM>(Department);

                if (DepartmentVm != null)
                {
                    int countCourses = Department.Course.Count;
                    if (countCourses != 0)
                    {
                        ViewBag.Role = "Admin";
                        ViewBag.Message = $"Department has dependents and cannot be deleted:  Course: {countCourses}";
                        return View("Error");
                    }
                    return View(DepartmentVm);
                }
            }

            ViewBag.Message = "Department was not found";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            {
                Department Department = _iCatalogService.GetDepartment(id);
                if (Department != null && Department.Course.Count == 0)
                {
                    _iCatalogService.DeleteDepartment(Department.Id);
                    return RedirectToAction("Info");
                }
            }

            ViewBag.Message = "Department was not found";
            return View("Error");
        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }
    }
}