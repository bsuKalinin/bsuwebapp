﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.CourseEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/Course")]
    [Authorize(Roles = "Administrator")]
    public class AdminCourseController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMembershipService _iMembershipService;
        private readonly IMapper _iMapper;

        public AdminCourseController(ICatalogService iCatalogService,
            IMapper iMapper, IMembershipService imembershipService)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
            _iMembershipService = imembershipService;
        }

        [Route("Info")]
        public IActionResult Info()
        {
           // string id = User.Claims.Where(x => x.Type == ClaimTypes.Name).First().Value;
           IEnumerable<CourseVM> CourseVms =
               _iMapper.Map<IEnumerable<Course>, IEnumerable<CourseVM>>(_iCatalogService.GetAllCourse());
               // .Where(x => x.LecturerCourse.Where(y => y.Email.ToString() == id).Count()
                 //           > 0));
            return View("Info", CourseVms);
        }

        [Route("Create")]
        [HttpGet]
        public IActionResult Create()
        {
            ViewData.Model = new CourseVM
            {
                Departments = new SelectList(_iCatalogService.GetAllDepartment(), "Id", "Name"),
                Lecturers = new List<LecturerVM>(),
                Department = null
            };

            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(CourseVM course)
        {
            if (!ModelState.IsValid)
            {
                course.Departments = new SelectList(_iCatalogService.GetAllDepartment(), "Id", "Name");
                return View(course);
            }

            course.Id = Guid.NewGuid();
            Course courseGr = _iMapper.Map<CourseVM, Course>(course);

            try
            {
                _iCatalogService.AddCourse(courseGr);
                return RedirectToAction("Info");
            }
            catch
            {
                ViewBag.Message = "Something goes wrong!";
                return View("Error");
            }
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            Course course = _iCatalogService.GetCourse(id);
            CourseVM courseVm;
            try
            {
                courseVm = _iMapper.Map<Course, CourseVM>(course);
                courseVm.Departments = new SelectList(
                    _iCatalogService.GetAllDepartment().Select(t => new {t.Id, t.Name}),
                    "Id", "Name");
                ViewData.Model = courseVm;
            }
            catch
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }

            return View(courseVm);
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(CourseVM courseVm)
        {
            if (!ModelState.IsValid)
            {
                courseVm.Departments = new SelectList(
                    _iCatalogService.GetAllDepartment().Select(t => new {t.Id, t.Name}),
                    "Id", "Name");
                return View(courseVm);
            }

            try
            {
                Course course = _iMapper.Map<CourseVM, Course>(courseVm);
                _iCatalogService.EditCourse(course);
                return RedirectToAction("Info");
            }

            catch
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            {
                Course course = _iCatalogService.GetCourse(id);
                CourseVM courseVm = _iMapper.Map<Course, CourseVM>(course);

                if (courseVm != null)
                {
                    int countCourses = course.LecturerCourse.Count;
                    if (countCourses != 0)
                    {
                        ViewBag.Message = $"Course has dependents and cannot be deleted:  Course: {countCourses}";
                        return View("Error");
                    }

                    return View(courseVm);
                }
            }

            ViewBag.Message = "Course was not found";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            {
                Course course = _iCatalogService.GetCourse(id);
                if (course != null && course.LecturerCourse.Count == 0)
                {
                    _iCatalogService.DeleteCourse(course.Id);
                    return RedirectToAction("Info");
                }
            }

            ViewBag.Message = "Course was not found";
            return View("Error");
        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }

        [Route("EditLecturer/{id}")]
        [HttpGet]
        public IActionResult EditLecturer(Guid id)
        {
            Course course = _iCatalogService.GetCourse(id);
            try
            {
                CourseVM courseVm = _iMapper.Map<Course, CourseVM>(course);

                courseVm.AllTheLecturers = new SelectList(_iCatalogService.GetAllLecturer()
                    .Except(_iCatalogService.GetCourseLecturers(course))
                    .Select(t => new {Id = t.UserId, FullName = $"{t.User.Name} {t.User.Surname}"}), "Id", "FullName");
                ViewData.Model = courseVm;
            }

            catch (Exception)
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }

            if (course != null)
            {
                return View();
            }

            ViewBag.Message = "Course was not found";
            return View("Error");
        }


        [Route("EditLecturer/DeleteLecturer")]
        public IActionResult DeleteLecturer(Guid courseId, Guid LecturerId)
        {
            _iCatalogService.DeleteCourseLecturer(_iCatalogService.GetCourse(courseId),
                _iCatalogService.GetLecturer(LecturerId));
            return RedirectToAction("EditLecturer", new {id = courseId});
        }

        [Route("EditLecturer/AddLecturer")]
        public IActionResult AddLecturer(Guid courseId, CourseVM course)
        {
            course.Id = courseId;
            _iCatalogService.AddLecturerCourse(_iCatalogService.GetCourse(course.Id),
                _iCatalogService.GetLecturer(course.SelectedLecturer));
            return RedirectToAction("EditLecturer", new {id = course.Id});
        }

        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {
            Course course = _iCatalogService.GetCourse(id);
            ViewData.Model = _iMapper.Map<Course, CourseVM>(course);
            if (course is null)
            {
                ViewBag.Message = "Course was not found";
                return View("Error");
            }

            return View();
        }
    }
}