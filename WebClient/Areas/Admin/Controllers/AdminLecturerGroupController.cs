﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/LecturerGroup")]
    [Authorize(Roles = "Administrator")]
    public class AdminLecturerGroupController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMapper _iMapper;

        public AdminLecturerGroupController(ICatalogService iCatalogService, IMapper iMapper)
        {
            _iCatalogService = iCatalogService;
            _iMapper = iMapper;
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<LecturerGroupVM> LecturerGroupVMs =
               _iMapper.Map<IEnumerable<LecturerGroup>, IEnumerable<LecturerGroupVM>>(_iCatalogService.GetAllLecturerGroup());
            return View("Info", LecturerGroupVMs);
        }

        [Route("Create")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(LecturerGroupVM LecturerGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(LecturerGroup);
            }
            LecturerGroup.Id = Guid.NewGuid();
            LecturerGroup LecturerGr = _iMapper.Map<LecturerGroupVM, LecturerGroup>(LecturerGroup);
            try
            {
                _iCatalogService.AddLecturerGroup(LecturerGr);
                return RedirectToAction("Info");

            }
            catch
            {

                ViewBag.Role = "Admin";
                ViewBag.Message = $"Lecturer Group with name {LecturerGr.Name} is already existed";
                return View("Error");
            }

        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            LecturerGroup LecturerGroup = _iCatalogService.GetLecturerGroup(id);
            LecturerGroupVM LecturerGr = _iMapper.Map<LecturerGroup, LecturerGroupVM>(LecturerGroup);
            if (LecturerGroup != null)
            {
                return View(LecturerGr);
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Lecturer Group was not found";
            return View("Error");
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(LecturerGroupVM LecturerGroup)
        {
            if (!ModelState.IsValid)
            {
                return View(LecturerGroup);
            }
            LecturerGroup LecturerGr = _iMapper.Map<LecturerGroupVM, LecturerGroup>(LecturerGroup);
            try
            {
                _iCatalogService.EditLecturerGroup(LecturerGr);
                return RedirectToAction("Info");
            }
            catch
            {
                ViewBag.Role = "Admin";
                ViewBag.Message = $"Department with name {LecturerGr.Name} is already existed";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            {
                LecturerGroup LecturerGroup = _iCatalogService.GetLecturerGroup(id);
                LecturerGroupVM LecturerGroupVm = _iMapper.Map<LecturerGroup, LecturerGroupVM>(LecturerGroup);

                if (LecturerGroupVm != null)
                {
                    int countLecturers = LecturerGroup.Lecturer.Count;
                    if (countLecturers != 0)
                    {
                        ViewBag.Role = "Admin";
                        ViewBag.Message = $"Department has dependents and cannot be deleted:  Course: {countLecturers}";
                        return View("Error");
                    }
                    return View(LecturerGroupVm);
                }
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Lecturer group was not found";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {

            LecturerGroup LecturerGroup = _iCatalogService.GetLecturerGroup(id);
            if (LecturerGroup != null && LecturerGroup.Lecturer.Count == 0)
            {
                _iCatalogService.DeleteLecturerGroup(LecturerGroup.Id);
                return RedirectToAction("Info");
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = "Lecturer group was not found";
            return View("Error");
        }

        [Route("Error")]
        public IActionResult Error()
        {
            return View("Error");
        }
    }
}