﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BLL.Contract;
using CMSys_project.ViewModels;
using Domain.Entities.UserEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CMSys_project.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/lecturer")]
    [Authorize(Roles = "Administrator")]

    public class AdminLecturerController : Controller
    {
        private readonly ICatalogService _iCatalogService;
        private readonly IMembershipService _iMembershipService;
        private readonly IMapper _iMapper;

        public AdminLecturerController(ICatalogService iCatalogService, IMapper iMapper, IMembershipService iMembershipService)
        {
            _iCatalogService = iCatalogService;
            _iMembershipService = iMembershipService;
            _iMapper = iMapper;
        }

        [Route("Create")]
        public IActionResult Create()
        {
            ViewData.Model = new LecturerVM
            {
                Groups = new SelectList(_iCatalogService.GetAllLecturerGroup(), "Id", "Name"),
                Group = null,
                NonLecturers = new SelectList(_iCatalogService.GetNonLecturers()
                    .Select(u => new { u.Id, FullName = u.Name + " " + u.Surname }), "Id", "FullName")
            };

            return View();
        }

        [Route("Create")]
        [HttpPost]
        public IActionResult Create(LecturerVM lecturer)
        {
            if (!ModelState.IsValid)
            {
                return View(lecturer);
            }
            Lecturer newLecturer = _iMapper.Map<LecturerVM, Lecturer>(lecturer);

            try
            {
                _iCatalogService.AddLecturer(newLecturer);
                return RedirectToAction("Info");
            }
            catch
            {

                ViewBag.Message = "Something goes wrong!";
                return View("Error");
            }
        }

        [Route("Info")]
        public IActionResult Info()
        {
            IEnumerable<LecturerVM> LecturerVMs =
                _iMapper.Map<IEnumerable<Lecturer>, IEnumerable<LecturerVM>>(_iCatalogService.GetAllLecturer());

                return View("Info", LecturerVMs);
                
        }

        [Route("Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            LecturerVM LecturerVm;
            Lecturer Lecturer;
            try
            {
                Lecturer = _iCatalogService.GetLecturer(id);
                LecturerVm = _iMapper.Map<Lecturer, LecturerVM>(Lecturer);

                LecturerVm.NonLecturers = new SelectList(
                    _iCatalogService.GetAllLecturerGroup().Select(t => new { t.Id, t.Name }),
                    "Id", "Name");
            }
            catch (Exception)
            {

                ViewBag.Message = "lecturer was not found!";
                return View("Error");
            }

            ViewData.Model = LecturerVm;

            if (Lecturer != null)
            {
                return View(LecturerVm);
            }

            ViewBag.Message = "lecturer was not found!";
            return View("Error");
        }

        [Route("Edit/{id}")]
        [HttpPost]
        public IActionResult Edit(LecturerVM LecturerVm)
        {
            if (!ModelState.IsValid)
            {
                return View(LecturerVm);
            }

            try
            {
                Lecturer Lecturer = _iMapper.Map<LecturerVM, Lecturer>(LecturerVm);
                Lecturer.UserId = LecturerVm.Id;
                _iCatalogService.EditLecturer(Lecturer);
                return RedirectToAction("Info");
            }
            catch
            {

                ViewBag.Message = "Something goes wrong";
                return View("Error");
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(Guid id)
        {
            Lecturer Lecturer = _iCatalogService.GetLecturer(id);
            LecturerVM LecturerVm = _iMapper.Map<Lecturer, LecturerVM>(Lecturer);

            if (LecturerVm == null)
            {

                ViewBag.Message = "lecturer was not found!";
                return View("Error");
            }

            int countCourses = _iCatalogService.GetAllCourse().SelectMany(course => course.LecturerCourse)
                .Count(LecturerCourse => LecturerCourse.Lecturer.UserId.Equals(Lecturer.UserId));

            if (countCourses == 0)
            {
                return View(LecturerVm);
            }

            ViewBag.Role = "Admin";
            ViewBag.Message = $"lecturer has dependents and cannot be deleted:  Course: {countCourses}";
            return View("Error");
        }

        [Route("Delete/{id}")]
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            {
                Lecturer Lecturer = _iCatalogService.GetLecturer(id);
                if (Lecturer != null)
                {
                    _iCatalogService.DeleteLecturer(Lecturer.UserId);
                    return RedirectToAction("Info");
                }
            }

            ViewBag.Message = "lecturer was not found";
            return View("Error");
        }

        [Route("Profile/{id}")]
        public IActionResult Profile(Guid id)
        {

            Lecturer Lecturer = _iCatalogService.GetLecturer(id);
            ViewData.Model = _iMapper.Map<Lecturer, LecturerVM>(Lecturer);
            if (Lecturer is null)
            {
                ViewBag.Message = "lecturer was not found";
                return View("Error");
            }

            return View();
        }
    }
}