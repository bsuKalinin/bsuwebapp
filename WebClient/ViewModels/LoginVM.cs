﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CMSys_project.ViewModels
{
    // ReSharper disable once InconsistentNaming
    public class LoginVM
    {
        [EmailAddress]
        [Required]
        [StringLength(128, ErrorMessage = "Wrong length!")]
        public string Email { get; set; }
        [Required]
        [StringLength(32, ErrorMessage = "Wrong length!")]
        public string Password { get; set; }
    }
}
