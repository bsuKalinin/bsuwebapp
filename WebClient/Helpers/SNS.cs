﻿using System;
using System.Net;
using System.Net.Mail;
using Domain.Entities.CourseEntities;

namespace CMSys_project.Helpers
{
    public static class SNS
    {
        public static async void SendMessageAsync(string mailSender, string mailReceiving, string mailUser,
            string fullName, string faculty, string courseName, string mailSenderPass)
        {
            CheckArgs(mailSender);
            CheckArgs(mailReceiving);
            CheckArgs(mailSenderPass);

            MailAddress from = new MailAddress(mailSender);
            MailAddress to = new MailAddress(mailReceiving);

            MailMessage mailMessage = new MailMessage(from, to)
            {
                Subject = "New subscriber!",
                Body = $"{fullName} from {faculty} has subscribed to your course {courseName}, email of new student is {mailUser}"
            };

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(mailSender, mailSenderPass),
                EnableSsl = true
            };

            await smtp.SendMailAsync(mailMessage);
        }

        private static void CheckArgs(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
        }
    }
}