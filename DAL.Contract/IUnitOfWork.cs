﻿using System;
using DAL.Contract.IRepositories;

namespace DAL.Contract
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IDepartmentRepository Departments { get; }
        ICourseRepository Courses { get; }
        IRoleRepository Roles { get; }
        ILecturerGroupRepository LecturerGroups { get; }
        ILecturerRepository Lecturers { get; }
        void Commit();
    }
}