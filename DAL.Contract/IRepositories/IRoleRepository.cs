﻿using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface IRoleRepository : IRepository<Role, int>
    {
    }
}