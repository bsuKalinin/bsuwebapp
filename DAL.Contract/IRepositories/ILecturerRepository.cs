﻿using System;
using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface ILecturerRepository : IRepository<Lecturer, Guid>
    {
    }
}