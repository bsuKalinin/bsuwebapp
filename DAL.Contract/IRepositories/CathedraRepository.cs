﻿using System;
using Domain.Entities.CourseEntities;

namespace DAL.Contract.IRepositories
{
    public interface IDepartmentRepository : IRepository<Department, Guid>
    {
    }
}