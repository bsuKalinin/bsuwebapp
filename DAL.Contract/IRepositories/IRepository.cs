﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain;

namespace DAL.Contract.IRepositories
{
    public interface IRepository<T, in TypeId> where T : Entity
    {
        IEnumerable<T> GetAll();
        T Get(TypeId id);
        IQueryable<T> Get(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(T entity);
        void Delete(TypeId id);
    }
}