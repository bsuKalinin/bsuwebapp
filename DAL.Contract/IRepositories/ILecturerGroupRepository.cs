﻿using System;
using Domain.Entities.UserEntities;

namespace DAL.Contract.IRepositories
{
    public interface ILecturerGroupRepository : IRepository<LecturerGroup, Guid>
    {
    }
}