﻿using System;
using Domain.Entities.UserEntities;

namespace Domain.Entities.CourseEntities
{
    public class LecturerCourse
    {
        public Guid UserId { get; set; }
        public virtual Lecturer Lecturer { get; set; }

        public Guid CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}