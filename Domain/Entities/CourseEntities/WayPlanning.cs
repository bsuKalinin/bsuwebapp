﻿namespace Domain.Entities.CourseEntities
{
    public enum WayPlanning
    {
        ByDemand,
        BySchedule
    }
}