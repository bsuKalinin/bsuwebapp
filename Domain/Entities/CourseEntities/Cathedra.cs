﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.CourseEntities
{
    public class Department : Entity<Guid>
    {
        public Department()
        {
            Course = new HashSet<Course>();
        }

        public string Name { get; set; }

        public virtual ICollection<Course> Course { get; set; }
    }
}