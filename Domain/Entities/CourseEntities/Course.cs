﻿using System;
using System.Collections.Generic;
using Domain.Entities.UserEntities;

namespace Domain.Entities.CourseEntities
{
    public class Course : Entity<Guid>
    {
        public Course()
        {
            LecturerCourse = new HashSet<LecturerCourse>();
            UserCourse = new HashSet<UserCourse>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsNew { get; set; }
        public CourseType Type { get; set; }
        public WayPlanning WayPlanning { get; set; }
        public Guid GroupCourseId { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<LecturerCourse> LecturerCourse { get; set; }
        public virtual ICollection<UserCourse> UserCourse { get; set; }

    }
}