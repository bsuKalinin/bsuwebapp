﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.UserEntities
{
    public class LecturerGroup : Entity<Guid>
    {
        public LecturerGroup()
        {
            Lecturer = new HashSet<Lecturer>();
        }

        public string Name { get; set; }

        public virtual ICollection<Lecturer> Lecturer { get; set; }
    }
}