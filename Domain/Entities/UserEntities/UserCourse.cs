﻿using System;
using Domain.Entities.CourseEntities;

namespace Domain.Entities.UserEntities
{
    public class UserCourse
    {
        public string UserEmail { get; set; }
        public virtual User User { get; set; }

        public Guid CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}