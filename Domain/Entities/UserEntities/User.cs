﻿using System;
using System.Collections.Generic;

namespace Domain.Entities.UserEntities
{
    public class User : Entity<Guid>
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
            UserCourse = new HashSet<UserCourse>();
        }

        public string Email { get; set; }
        public string PassHash { get; set; }
        public string PassSalt { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime WorkBegin { get; set; }
        public DateTime? WorkEnd { get; set; }
        public string Department { get; set; }
        public string Faculty { get; set; }
        public string Position { get; set; }
        public byte[] Photo { get; set; }

        public virtual Lecturer Lecturer { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
        public virtual ICollection<UserCourse> UserCourse { get; set; }
    }
}