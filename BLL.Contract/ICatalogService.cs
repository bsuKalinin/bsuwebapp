﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace BLL.Contract
{
    public interface ICatalogService
    {
        #region Department

        void AddDepartment(Department Department);
        void DeleteDepartment(Guid id);
        void EditDepartment(Department Department);
        IEnumerable<Department> GetAllDepartment();
        Department GetDepartment(Guid id);

        #endregion

        #region LecturerGroup

        void AddLecturerGroup(LecturerGroup LecturerGroup);
        void DeleteLecturerGroup(Guid id);
        void EditLecturerGroup(LecturerGroup LecturerGroup);
        IEnumerable<LecturerGroup> GetAllLecturerGroup();
        LecturerGroup GetLecturerGroup(Guid id);

        #endregion

        #region Lecturer

        void AddLecturer(Lecturer Lecturer);
        void DeleteLecturer(Guid id);
        void EditLecturer(Lecturer Lecturer);
        IEnumerable<Lecturer> GetAllLecturer();
        Lecturer GetLecturer(Guid id);
        IEnumerable<User> GetNonLecturers();

        #endregion

        #region Course

        void AddCourse(Course course);
        void DeleteCourse(Guid id);
        void EditCourse(Course course);
        IEnumerable<Course> GetAllCourse();
        IQueryable<Course> GetCourses(Expression <Func<Course, bool>> expression);
        Course GetCourse(Guid id);
        IEnumerable<Lecturer> GetCourseLecturers(Course course);
        void DeleteCourseLecturer(Course course, Lecturer Lecturer);
        void AddLecturerCourse(Course course, Lecturer Lecturer);

        #endregion

    }
}