﻿using System;
using System.Collections.Generic;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace BLL.Contract
{
    public interface IMembershipService
    {
        #region User

        void EditUser(User user, List<UserRole> userRole);
        void AddCourse(User userV, Course courseV);
        IEnumerable<User> GetAllUsers();
        User GetUser(Guid id);
        IEnumerable<Role> GetAllRoles();
        bool EqualPasswords(User user, string password);
        Role GetRole(int id);

        #endregion
    }
}