﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BLL.Contract;
using DAL.Contract;
using Domain.Entities.CourseEntities;
using Domain.Entities.UserEntities;

namespace BLL.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CatalogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        #region Department

        public void AddDepartment(Department Department)
        {
            if (Department is null)
            {
                throw new ArgumentNullException(nameof(Department));
            }

            _unitOfWork.Departments.Add(Department);
            _unitOfWork.Commit();
        }

        public void DeleteDepartment(Guid id)
        {
            _unitOfWork.Departments.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditDepartment(Department Department)
        {
            if (Department is null)
            {
                throw new ArgumentNullException(nameof(Department));
            }

            Department courseGr = _unitOfWork.Departments.Get(Department.Id);
            if (courseGr is null)
            {
                throw new ArgumentNullException(nameof(courseGr));
            }
            courseGr.Name = Department.Name;
            _unitOfWork.Commit();
        }

        public IEnumerable<Department> GetAllDepartment()
        {
            return _unitOfWork.Departments.GetAll();
        }

        public Department GetDepartment(Guid id)
        {
            return _unitOfWork.Departments.Get(id);
        }

        #endregion

        #region LecturerGroup

        public void AddLecturerGroup(LecturerGroup LecturerGroup)
        {
            if (LecturerGroup is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.LecturerGroups.Add(LecturerGroup);
            _unitOfWork.Commit();
        }

        public void DeleteLecturerGroup(Guid id)
        {
            _unitOfWork.LecturerGroups.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditLecturerGroup(LecturerGroup LecturerGroup)
        {
            if (LecturerGroup is null)
            {
                throw new ArgumentNullException();
            }

            LecturerGroup LecturerGr = _unitOfWork.LecturerGroups.Get(LecturerGroup.Id);
            if (LecturerGr is null)
            {
                throw new ArgumentNullException(nameof(LecturerGr));
            }
            LecturerGr.Name = LecturerGroup.Name;
            _unitOfWork.Commit();
        }

        public IEnumerable<LecturerGroup> GetAllLecturerGroup()
        {
            return _unitOfWork.LecturerGroups.GetAll();
        }

        public LecturerGroup GetLecturerGroup(Guid id)
        {
            return _unitOfWork.LecturerGroups.Get(id);
        }

        #endregion

        #region Lecturer

        public void AddLecturer(Lecturer Lecturer)
        {
            if (Lecturer is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.Lecturers.Add(Lecturer);
            _unitOfWork.Commit();
        }

        public void DeleteLecturer(Guid id)
        {
            _unitOfWork.Lecturers.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditLecturer(Lecturer Lecturer)
        {
            if (Lecturer is null)
            {
                throw new ArgumentNullException();
            }

            Lecturer newLecturer = _unitOfWork.Lecturers.Get(Lecturer.UserId);
            if (newLecturer is null)
            {
                throw new ArgumentNullException(nameof(newLecturer));
            }
            newLecturer.GroupId = Lecturer.GroupId;
            newLecturer.About = Lecturer.About;
            _unitOfWork.Commit();
        }

        public IEnumerable<Lecturer> GetAllLecturer()
        {
            return _unitOfWork.Lecturers.GetAll();
        }

        public Lecturer GetLecturer(Guid id)
        {
            return _unitOfWork.Lecturers.Get(id);
        }

        public IEnumerable<User> GetNonLecturers()
        {
            return _unitOfWork.Users.Get(z => z.Lecturer.Equals(null));
        }

        #endregion

        #region Course

        public void AddCourse(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }

            _unitOfWork.Courses.Add(course);
            _unitOfWork.Commit();
        }

        public void DeleteCourse(Guid id)
        {
            _unitOfWork.Courses.Delete(id);
            _unitOfWork.Commit();
        }

        public void EditCourse(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }

            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException();
            }
            newCourse.GroupCourseId = course.GroupCourseId;
            newCourse.WayPlanning = course.WayPlanning;
            newCourse.Name = course.Name;
            newCourse.IsNew = course.IsNew;
            newCourse.Type = course.Type;
            newCourse.Description = course.Description;
            _unitOfWork.Commit();
        }

        public IEnumerable<Course> GetAllCourse()
        {
            return _unitOfWork.Courses.GetAll();
        }

        public IQueryable<Course> GetCourses(Expression<Func<Course, bool>> predicate)
        {
            return _unitOfWork.Courses.Get(predicate);
        }
        public Course GetCourse(Guid id)
        {
            return _unitOfWork.Courses.Get(id);
        }

        public IEnumerable<Lecturer> GetCourseLecturers(Course course)
        {
            if (course is null)
            {
                throw new ArgumentNullException();
            }
            return course.LecturerCourse.Select(x => x.Lecturer);
        }

        public void DeleteCourseLecturer(Course course, Lecturer Lecturer)
        {
            if (course is null)
            {
                throw new ArgumentNullException(nameof(course));
            }

            if (Lecturer is null)
            {
                throw new ArgumentNullException(nameof(Lecturer));
            }
            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException(nameof(newCourse));
            }
            newCourse.LecturerCourse.Remove(newCourse.LecturerCourse.First(x => x.UserId.Equals(Lecturer.UserId)));
            _unitOfWork.Commit();
        }

        public void AddLecturerCourse(Course course, Lecturer Lecturer)
        {
            if (course is null)
            {
                throw new ArgumentNullException(nameof(course));
            }

            if (Lecturer is null)
            {
                throw new ArgumentNullException(nameof(Lecturer));
            }
            Course newCourse = _unitOfWork.Courses.Get(course.Id);
            if (newCourse is null)
            {
                throw new ArgumentNullException(nameof(newCourse));
            }
            newCourse.LecturerCourse.Add(new LecturerCourse
                {Course = newCourse, CourseId = newCourse.Id, Lecturer = Lecturer, UserId = Lecturer.UserId});
            _unitOfWork.Commit();
        }

        #endregion

     
    }
}