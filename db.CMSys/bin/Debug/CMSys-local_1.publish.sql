﻿/*
Скрипт развертывания для CMSys

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "CMSys"
:setvar DefaultFilePrefix "CMSys"
:setvar DefaultDataPath "E:\SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "E:\SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [master];


GO

IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Выполняется создание $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [$(DatabaseName)], FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_Primary.mdf')
    LOG ON (NAME = [$(DatabaseName)_log], FILENAME = N'$(DefaultLogPath)$(DefaultFilePrefix)_Primary.ldf') COLLATE SQL_Latin1_General_CP1_CI_AS
GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY FULL,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE,
                SUPPLEMENTAL_LOGGING OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'Параметры базы данных изменить нельзя. Применить эти параметры может только пользователь SysAdmin.';
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET HONOR_BROKER_PRIORITY OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'Параметры базы данных изменить нельзя. Применить эти параметры может только пользователь SysAdmin.';
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET FILESTREAM(NON_TRANSACTED_ACCESS = OFF),
                CONTAINMENT = NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF),
                MEMORY_OPTIMIZED_ELEVATE_TO_SNAPSHOT = OFF,
                DELAYED_DURABILITY = DISABLED 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (QUERY_CAPTURE_MODE = ALL, DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_PLANS_PER_QUERY = 200, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367), MAX_STORAGE_SIZE_MB = 100) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE = OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
        ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET TEMPORAL_HISTORY_RETENTION ON 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO
PRINT N'Выполняется создание [dbo].[Course]...';


GO
CREATE TABLE [dbo].[Course] (
    [CourseId]      UNIQUEIDENTIFIER NOT NULL,
    [Name]          NVARCHAR (64)    NOT NULL,
    [Description]   NVARCHAR (4000)  NULL,
    [IsNew]         BIT              NOT NULL,
    [Type]          INT              NOT NULL,
    [WayPlanning]   INT              NOT NULL,
    [GroupCourseId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Course_CourseId] PRIMARY KEY CLUSTERED ([CourseId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[CourseGroup]...';


GO
CREATE TABLE [dbo].[CourseGroup] (
    [CourseGroupId] UNIQUEIDENTIFIER NOT NULL,
    [Name]          NVARCHAR (32)    NOT NULL,
    CONSTRAINT [PK_CourseGroup_CourseGroupId] PRIMARY KEY CLUSTERED ([CourseGroupId] ASC),
    CONSTRAINT [AK_CourseGroup_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[Lecturer]...';


GO
CREATE TABLE [dbo].[Lecturer] (
    [UserId]  UNIQUEIDENTIFIER NOT NULL,
    [About]   NVARCHAR (4000)  NULL,
    [GroupId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Lecturer_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[LecturerCourse_xref]...';


GO
CREATE TABLE [dbo].[LecturerCourse_xref] (
    [UserId]   UNIQUEIDENTIFIER NOT NULL,
    [CourseId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LecturerCourse_xref_UserId_CourseId] PRIMARY KEY CLUSTERED ([UserId] ASC, [CourseId] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[LecturerGroup]...';


GO
CREATE TABLE [dbo].[LecturerGroup] (
    [LecturerGroupId] UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (32)    NOT NULL,
    CONSTRAINT [PK_LecturerGroup_LecturerGroupId] PRIMARY KEY CLUSTERED ([LecturerGroupId] ASC),
    CONSTRAINT [AK_LecturerGroup_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[Role]...';


GO
CREATE TABLE [dbo].[Role] (
    [RoleId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]   NVARCHAR (32) NOT NULL,
    CONSTRAINT [PK_Role_RoleId] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    CONSTRAINT [AK_Role_Name] UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[User]...';


GO
CREATE TABLE [dbo].[User] (
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [Email]     NVARCHAR (128)   NOT NULL,
    [PassHash]  NVARCHAR (128)   NOT NULL,
    [PassSalt]  NVARCHAR (128)   NOT NULL,
    [Name]      NVARCHAR (128)   NOT NULL,
    [Surname]   NVARCHAR (128)   NOT NULL,
    [WorkBegin] DATE             NOT NULL,
    [WorkEnd]   DATE             NULL,
    [Faculty]   NVARCHAR (128)   NULL,
    CONSTRAINT [PK_User_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [AK_User_Email] UNIQUE NONCLUSTERED ([Email] ASC)
);


GO
PRINT N'Выполняется создание [dbo].[UserRole_xref]...';


GO
CREATE TABLE [dbo].[UserRole_xref] (
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId] INT              NOT NULL,
    CONSTRAINT [PK_UserRole_xref_UserId_RoleId] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC)
);


GO
PRINT N'Выполняется создание ограничение без названия для [dbo].[Course]...';


GO
ALTER TABLE [dbo].[Course]
    ADD DEFAULT 0 FOR [IsNew];


GO
PRINT N'Выполняется создание [dbo].[FK_Course_GroupCourseId]...';


GO
ALTER TABLE [dbo].[Course]
    ADD CONSTRAINT [FK_Course_GroupCourseId] FOREIGN KEY ([GroupCourseId]) REFERENCES [dbo].[CourseGroup] ([CourseGroupId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_Lecturer_User_UserId]...';


GO
ALTER TABLE [dbo].[Lecturer]
    ADD CONSTRAINT [FK_Lecturer_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_Lecturer_LecturerGroup_GroupId]...';


GO
ALTER TABLE [dbo].[Lecturer]
    ADD CONSTRAINT [FK_Lecturer_LecturerGroup_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[LecturerGroup] ([LecturerGroupId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_LecturerCourse_xref_Trainer_UserId]...';


GO
ALTER TABLE [dbo].[LecturerCourse_xref]
    ADD CONSTRAINT [FK_LecturerCourse_xref_Trainer_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Lecturer] ([UserId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_LecturerCourse_xref_Course_CourseId]...';


GO
ALTER TABLE [dbo].[LecturerCourse_xref]
    ADD CONSTRAINT [FK_LecturerCourse_xref_Course_CourseId] FOREIGN KEY ([CourseId]) REFERENCES [dbo].[Course] ([CourseId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_UserRole_xref_User_UserId]...';


GO
ALTER TABLE [dbo].[UserRole_xref]
    ADD CONSTRAINT [FK_UserRole_xref_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE;


GO
PRINT N'Выполняется создание [dbo].[FK_UserRole_xref_Role_RoleId]...';


GO
ALTER TABLE [dbo].[UserRole_xref]
    ADD CONSTRAINT [FK_UserRole_xref_Role_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([RoleId]) ON DELETE CASCADE;


GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Role
INSERT INTO [dbo].[Role] ([Name])
VALUES  ('Administrator'),
		('User')
GO
--User
INSERT INTO [dbo].[User] ([UserId], [Email], [PassHash], [PassSalt], [Name], [Surname], [WorkBegin],
						 [Department], [Office], [Position])
VALUES
	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB',
		'someemail@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj',
		'Anton', 'Krulov', '2015-04-05',
		'Engineer', 'Chapaeva, 5', '.NET'),

	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F61577CB',
		'anotheremail@gmail.com',
		'$2y$10$0Y5Yzt6sRRpIIXLGCXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGRls-ANHuA_S',
		'Uri', 'Belov', '2012-04-05',
		'Engineer', 'Chapaeva, 5', '.NET') ,

	(	'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB',
		'onemoreemail@gmail.com',
		'$2y$10$0Y5Yzt6sRyfjhfjXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGhjhjjs-ANHuA_S', 
		'Max', 'Skripnik', '2017-04-20',
		'Driver', 'Zetkin, 51', NULL),
	
	(	'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB',
		'nothisemail@gmail.com',
		'$2y$10$0jksdksRyfjhfjXFVuu7IQohjgh5JOODPBa5Cvmo7MgC',
		'Y3hGhjhjjs-ANknuA_S',
		'Anrdey', 'Pugovkin', '2008-10-13',
		'Driver', 'Chapaeva, 5' , 'Java'),
		
	(	'C8BCE01D-52AB-2E8F-7CFA-46A5C61000CB', 
		'notmyemail@gmail.com',
		'ElIxrl57Uaxxtzf9R9y1HNCp+X9EWdikBp7EmMplvIs=',
		'rxRaXafnh8BdDjGn2', 
		'Anna', 'Merkush', '2019-05-10',
		'Engineer', 'Chapaeva, 5', 'BI') 
GO
--UserRole_xref
DELETE FROM [dbo].UserRole_xref
INSERT INTO [dbo].[UserRole_xref] ([UserId], [RoleId])
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Administrator')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F61577CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Administrator')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-52AB-2E8F-7CFA-46A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
GO
--GroupTrainers
INSERT INTO [dbo].[LecturerGroup] ([LecturerGroupId], [Name])
VALUES  ('7F2898AC-5644-4672-A3E0-CC8364571D63', 'Java'),
		('131C3018-48D9-4459-8EEC-C516D13CE504', 'DB')
GO
--Trainer
INSERT INTO [dbo].[Lecturer] ([UserId], [About], [GroupId])
VALUES  ('C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB', 'Some info about me', '7F2898AC-5644-4672-A3E0-CC8364571D63'),
		('C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB', NULL, '131C3018-48D9-4459-8EEC-C516D13CE504')
GO
--GroupCourses
INSERT INTO [dbo].[CourseGroup] ([CourseGroupId], [Name])
VALUES  ('70D291B3-5665-47E7-9ADF-59C3BA5D5230', 'Java'),
		('DFB68B68-7687-4413-B408-8CBEE50EAA26', 'DB')
GO
--Course
INSERT INTO [dbo].[Course] ([CourseId], [Description], [GroupCourseId], [IsNew], [Name], [WayPlanning], [Type])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A','This course is for people, who wants to learn sql', '70D291B3-5665-47E7-9ADF-59C3BA5D5230',
		 0, 'Sql for dev', 0, 1),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'This course is for people, who wants to learn java', 'DFB68B68-7687-4413-B408-8CBEE50EAA26',
		 1, 'Java for begin', 1, 0)
GO
--TrainerCourse_xref
INSERT INTO [dbo].[LecturerCourse_xref] ([CourseId], [UserId])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A', 'C8BCE01D-53AB-4E8F-8CFA-49A4F61500CB'),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'C8BCE01D-53AB-4E8F-8CFA-49A4F62500CB')
GO

GO
DECLARE @VarDecimalSupported AS BIT;

SELECT @VarDecimalSupported = 0;

IF ((ServerProperty(N'EngineEdition') = 3)
    AND (((@@microsoftversion / power(2, 24) = 9)
          AND (@@microsoftversion & 0xffff >= 3024))
         OR ((@@microsoftversion / power(2, 24) = 10)
             AND (@@microsoftversion & 0xffff >= 1600))))
    SELECT @VarDecimalSupported = 1;

IF (@VarDecimalSupported > 0)
    BEGIN
        EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
    END


GO
PRINT N'Обновление завершено.';


GO
