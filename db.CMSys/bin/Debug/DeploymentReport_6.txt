﻿** Выделение
     Таблицы, которые будут перестроены
       Нет
     Кластеризованные индексы, которые будут удалены
       Нет
     Кластеризованные индексы, которые будут созданы
       Нет
     Возможные проблемы, связанные с данными
       Нет

** Действия пользователя
     Создать
       [dbo].[Course] (Таблица)
       [dbo].[CourseGroup] (Таблица)
       [dbo].[Role] (Таблица)
       [dbo].[Trainer] (Таблица)
       [dbo].[TrainerCourse_xref] (Таблица)
       [dbo].[TrainerGroup] (Таблица)
       [dbo].[User] (Таблица)
       [dbo].[UserRole_xref] (Таблица)
       Ограничение по умолчанию: ограничение без названия для [dbo].[Course] (Ограничение по умолчанию)
       [dbo].[FK_Course_GroupCourseId] (Внешний ключ)
       [dbo].[FK_Trainer_User_UserId] (Внешний ключ)
       [dbo].[FK_Trainer_TrainerGroup_GroupId] (Внешний ключ)
       [dbo].[FK_TrainerCourse_xref_Trainer_UserId] (Внешний ключ)
       [dbo].[FK_TrainerCourse_xref_Course_CourseId] (Внешний ключ)
       [dbo].[FK_UserRole_xref_User_UserId] (Внешний ключ)
       [dbo].[FK_UserRole_xref_Role_RoleId] (Внешний ключ)

** Корректирующие действия
