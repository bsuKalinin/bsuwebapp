﻿CREATE TABLE [dbo].[Department](
    [DepartmentId]     UNIQUEIDENTIFIER        NOT NULL,
    [Name]              NVARCHAR(32)         NOT NULL

    CONSTRAINT  [PK_Department_DepartmentId]        PRIMARY KEY      ([DepartmentId])
    CONSTRAINT  [AK_Department_Name]              UNIQUE      ([Name])
)
GO