﻿CREATE TABLE [dbo].[UserCourse_xref](
    [UserEmail]     nvarchar(128)        NOT NULL, 
    [CourseId]   UNIQUEIDENTIFIER                     NOT NULL

    CONSTRAINT  [PK_UserCourse_xref_UserEmail_CourseId]   PRIMARY KEY 
    (
        [UserEmail],
        [CourseId]
    )

    CONSTRAINT  [FK_UserCourse_xref_User_UserEmail]   FOREIGN KEY ([UserEmail])           REFERENCES [dbo].[User]([Email])  ON DELETE CASCADE
    CONSTRAINT  [FK_UserCourse_xref_Course_CourseId]   FOREIGN KEY ([CourseId])     REFERENCES [dbo].[Course]([CourseId])  ON DELETE CASCADE
)
GO