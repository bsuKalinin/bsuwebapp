﻿CREATE TABLE [dbo].[Lecturer](
    [UserId]      UNIQUEIDENTIFIER       NOT NULL, 
    [About]       NVARCHAR(4000),
    [GroupId]     UNIQUEIDENTIFIER       NOT NULL

    CONSTRAINT  [PK_Lecturer_UserId]           PRIMARY KEY ([UserId])

    CONSTRAINT  [FK_Lecturer_User_UserId]              FOREIGN KEY ([UserId])      REFERENCES  [dbo].[User]([UserId])                  ON DELETE CASCADE
    CONSTRAINT  [FK_Lecturer_LecturerGroup_GroupId]     FOREIGN KEY ([GroupId])     REFERENCES  [dbo].[LecturerGroup]([LecturerGroupId])  ON DELETE CASCADE

)
GO