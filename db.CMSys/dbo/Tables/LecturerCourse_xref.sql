﻿CREATE TABLE [dbo].[LecturerCourse_xref](
    [UserId]       UNIQUEIDENTIFIER        NOT NULL, 
    [CourseId]     UNIQUEIDENTIFIER        NOT NULL

    CONSTRAINT  [PK_LecturerCourse_xref_UserId_CourseId]   PRIMARY KEY 
    (
        [UserId],
        [CourseId]
    )

    CONSTRAINT  [FK_LecturerCourse_xref_Lecturer_UserId]    FOREIGN KEY ([UserId])       REFERENCES [dbo].[Lecturer]([UserId])   ON DELETE CASCADE
    CONSTRAINT  [FK_LecturerCourse_xref_Course_CourseId]   FOREIGN KEY ([CourseId])     REFERENCES [dbo].[Course]([CourseId])   ON DELETE CASCADE
)
GO