﻿CREATE TABLE [dbo].[LecturerGroup](
    [LecturerGroupId]     UNIQUEIDENTIFIER        NOT NULL, 
    [Name]               NVARCHAR(32)            NOT NULL

    CONSTRAINT  [PK_LecturerGroup_LecturerGroupId]   PRIMARY KEY ([LecturerGroupId])

    CONSTRAINT  [AK_LecturerGroup_Name]             UNIQUE        ([Name])
)
GO