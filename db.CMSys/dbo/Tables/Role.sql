﻿CREATE TABLE [dbo].[Role](
    [RoleId]     INT                NOT NULL   IDENTITY, 
    [Name]       NVARCHAR(32)       NOT NULL

    CONSTRAINT  [PK_Role_RoleId]   PRIMARY KEY ([RoleId])

    CONSTRAINT  [AK_Role_Name]     UNIQUE      ([Name])
)
GO