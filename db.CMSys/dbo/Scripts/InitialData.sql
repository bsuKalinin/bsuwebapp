﻿--Role
INSERT INTO [dbo].[Role] ([Name])
VALUES  ('Administrator'),
		('User'),
		('Lecturer')
GO
--User
INSERT INTO [dbo].[User] ([UserId], [Email], [PassHash], [PassSalt], [Name], [Surname], [WorkBegin],
						 [Department], [Faculty])
VALUES
	(	'20F3F06B-413F-41D3-91E9-4FB289FF4637',
		'admin@admin.ru',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj',
		'Anton', 'Krulov', '2015-04-05',
		NULL, NULL),

	(	'3629E95D-E09C-47A1-B08F-1EC67D0591F0',
		'lector1fromFamcs@mail.ru',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj',
		'Uri', 'Belov', '2012-04-05',
		'Computer Applications and Systems', 'FAMCS') ,

	(	'C45DB237-D861-458B-AFB3-B4AAEC1E6236',
		'lector2fromFamcs@mail.ru',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Max', 'Skripnik', '2017-04-20',
		'Computer Applications and Systems', 'FAMCS'),
	
	(	'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB',
		'lector1fromFir@mail.ru',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj',
		'Anrdey', 'Pugovkin', '2008-10-13',
		'Foreign languages', 'FIR'),
		
	(	'4C31AA11-0842-47EB-9C93-478145D8519F', 
		'student1famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Valeriy', 'Bogatko', '2016-05-10',
		'Computer Applications and Systems', 'FAMCS'),

		('4C31AA12-0842-47EB-9C93-478145D8519F', 
		'student2famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Darya', 'Belay', '2016-10-10',
		'Economy', 'FIR'),

		('4C31AA13-0842-47EB-9C93-478145D8519F', 
		'student3famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Inna', 'Golovkina', '2019-09-10',
		'Different economy', 'EF'),

		('4C31AA14-0842-47EB-9C93-478145D8519F', 
		'student4famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Ivan', 'Zhuk', '2015-01-08',
		'Something', 'IEF'),

		('4C31AA15-0842-47EB-9C93-478145D8519F', 
		'student5famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Nikolay', 'Ivanov', '2018-10-03',
		NULL, 'FAMCS'),

		('4C31AA16-0842-47EB-9C93-478145D8519F', 
		'student6famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Anna', 'Belgish', '2013-03-05',
		NULL, 'FAMCS'),

		('4C31AA17-0842-47EB-9C93-478145D8519F', 
		'student7famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Roman', 'Girin', '2017-08-10',
		'Applied mathematics', 'MMF'),

		('4C31AA18-0842-47EB-9C93-478145D8519F', 
		'student8famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Sergey', 'Krot', '2019-07-06',
		'Applied mathematics', 'MMF'),

		('4C31AA19-0842-47EB-9C93-478145D8519F', 
		'student9famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'James', 'Bond', '2019-02-10',
		NULL, 'FFSN'),

		('4C31AA20-0842-47EB-9C93-478145D8519F', 
		'student10famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Bulin', 'Kurin', '2016-12-12',
		'Applied culturology', 'FSK'),

		('4C31AA21-0842-47EB-9C93-478145D8519F', 
		'student11famcs@gmail.com',
		'OSk2GHvGxV025rpUqC2TBj6dJiPbLJJnOJGnDWiW9j8=',
		'62dRsOrrlZ5ubj', 
		'Lakina', 'Bakina', '2019-05-10',
		'Best department ever', 'FAMCS')
GO
----UserRole_xref
DELETE FROM [dbo].UserRole_xref
INSERT INTO [dbo].[UserRole_xref] ([UserId], [RoleId])
	(SELECT   '20F3F06B-413F-41D3-91E9-4FB289FF4637', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '20F3F06B-413F-41D3-91E9-4FB289FF4637', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Lecturer')
		UNION
	(SELECT   '20F3F06B-413F-41D3-91E9-4FB289FF4637', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Administrator')
		UNION
	(SELECT   '3629E95D-E09C-47A1-B08F-1EC67D0591F0', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '3629E95D-E09C-47A1-B08F-1EC67D0591F0', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Lecturer')
		UNION
	(SELECT   'C45DB237-D861-458B-AFB3-B4AAEC1E6236', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C45DB237-D861-458B-AFB3-B4AAEC1E6236', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Lecturer')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'Lecturer')
		UNION
	(SELECT   '4C31AA11-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA12-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA13-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA14-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA15-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA16-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA17-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA18-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA19-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA20-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')
		UNION
	(SELECT   '4C31AA21-0842-47EB-9C93-478145D8519F', [RoleId] FROM dbo.[Role] r WHERE r.[Name] = 'User')

GO
--GroupLecturers
INSERT INTO [dbo].[LecturerGroup] ([LecturerGroupId], [Name])
VALUES  ('7F2898AC-5644-4672-A3E0-CC8364571D63', 'Foreign languages'),
		('131C3018-48D9-4459-8EEC-C516D13CE504', 'CAS')
GO
--Lecturer
INSERT INTO [dbo].[Lecturer] ([UserId], [About], [GroupId])
VALUES  ('C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB', 'Some info about me', '7F2898AC-5644-4672-A3E0-CC8364571D63'),
		('3629E95D-E09C-47A1-B08F-1EC67D0591F0', 'Love FAMCS!!!', '131C3018-48D9-4459-8EEC-C516D13CE504'),
		('C45DB237-D861-458B-AFB3-B4AAEC1E6236', 'More info', '131C3018-48D9-4459-8EEC-C516D13CE504')

GO
--GroupCourses
INSERT INTO [dbo].[Department] ([DepartmentId], [Name])
VALUES  ('70D291B3-5665-47E7-9ADF-59C3BA5D5230', 'FAMCS'),
		('DFB68B68-7687-4413-B408-8CBEE50EAA26', 'FIR')
GO
--Course
INSERT INTO [dbo].[Course] ([CourseId], [Description], [GroupCourseId], [IsNew], [Name], [WayPlanning], [Type])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A','This course is for people, who wants to learn sql', '70D291B3-5665-47E7-9ADF-59C3BA5D5230',
		 0, 'Sql for dev', 0, 1),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'This course is for people, who wants to learn english', 'DFB68B68-7687-4413-B408-8CBEE50EAA26',
		 1, 'English for beginners', 1, 0),
		('01F4F097-3245-4869-A378-297B84AD60CB', 'This course is for people, who wants to learn german', 'DFB68B68-7687-4413-B408-8CBEE50EAA26',
		 1, 'German for beginners', 1, 0)
GO
--LecturerCourse_xref
INSERT INTO [dbo].[LecturerCourse_xref] ([CourseId], [UserId])
VALUES  ('DB01395C-2915-43A6-B9F1-ADA582F2155A', '3629E95D-E09C-47A1-B08F-1EC67D0591F0'),
		('DB01395C-2915-43A6-B9F1-ADA582F2155A', 'C45DB237-D861-458B-AFB3-B4AAEC1E6236'),
		('01F4F097-3245-4869-A378-297B84AD60CA', 'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB'),
		('01F4F097-3245-4869-A378-297B84AD60CB', 'C8BCE01D-53AB-4E8F-8CFA-49A5C61000CB')

GO

INSERT INTO [dbo].[UserCourse_xref] ([UserEmail], [CourseId])
VALUES  ('student5famcs@gmail.com', 'DB01395C-2915-43A6-B9F1-ADA582F2155A')

GO